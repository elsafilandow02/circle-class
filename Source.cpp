#include <stdio.h>
#include "circle.hpp"

void main()
{
	circle c;
	double rad;

	do {
		printf("Radius = ");
		scanf("%lf", &rad);

		if (rad == 0)
			return;

		c.setRadius(rad);

		printf("Circle with ");
		printf("Radius = %lf\n", c.getRadius());
		printf("then Area = %lf ", c.getArea());
		printf(" and Circumference = %lf\n", c.getCircumference());
	} while (1);
}