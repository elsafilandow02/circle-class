#include <math.h>

# define M_PIl          3.141592653589793238462643383279502884L /* pi */

class circle {
public:
	void setRadius(double r) {
		rad = r;
	}

	double getRadius() {
		return rad;
	}

	double getArea() {
		return ((double)M_PIl * rad * rad);
	}

	double getCircumference() {
		return ((double)M_PIl * rad * 2);
	}

private:
	double rad;
};